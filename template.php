<?php

/**
 * @file
 * Theme hooks for overriding output.
 */

/**
 * Implements hook_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function gridless_preprocess_html(&$variables) {

  // Add viewport metatag for mobile devices.
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    ),
  );
  drupal_add_html_head($meta_viewport, 'meta_viewport');

  // Set up IE meta tag to force IE rendering mode.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' =>  'IE=edge,chrome=1',
    ),
  );
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');

  // Add minimal fixes for Internet explorer.
  drupal_add_css(drupal_get_path('theme', 'gridless') . '/assets/css/ie.css', array(
    'group' => CSS_THEME,
    'browsers' => array('IE' => 'lt IE 9', '!IE' => FALSE),
    'preprocess' => FALSE
  ));
}
