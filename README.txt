
Gridless Boilerpoint Theme 7.x-1.x
==================================

Project Lead: [Micah Webner](http://drupal.org/project/user/40138)

This theme is based on [Gridless](http://thatcoolguy.github.com/gridless-boilerplate/),
an HTML5 and CSS3 boilerplate by thatcoolguy.

Thanks to Maria Buffa for recommending Gridless as a starting point.
